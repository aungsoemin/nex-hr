class CreateLeaves < ActiveRecord::Migration
  def change
    create_table :leaves do |t|
      t.date :requested_date
      t.date :start_date
      t.date :end_date
      t.integer :leave_type_id
      t.text :reason
      t.string :attachment
      t.integer :employee_id
      t.integer :first_approved_by_id
      t.integer :second_approved_by_id
      t.integer :leave_status_id

      t.timestamps null: false
    end
  end
end
