class CreateAttendanceSheets < ActiveRecord::Migration
  def change
    create_table :attendance_sheets do |t|
      t.integer :employee_id
      t.time :start_time
      t.time :end_time
      t.date :date
      t.integer :submit_by_id

      t.timestamps null: false
    end
  end
end
