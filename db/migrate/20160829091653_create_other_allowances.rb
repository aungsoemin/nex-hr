class CreateOtherAllowances < ActiveRecord::Migration
  def change
    create_table :other_allowances do |t|
    	t.integer :employee_id
    	t.string :name
    	t.integer :amount
    	t.date :allowance_month

      t.timestamps null: false
    end
  end
end