class CreateEmergencyPhones < ActiveRecord::Migration
  def change
    create_table :emergency_phones do |t|
    	t.string :name
    	t.string :phone 
    	t.integer :employee_id

      t.timestamps null: false
    end
  end
end
