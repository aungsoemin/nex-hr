class ChangeColumnInEmployeeTbl1 < ActiveRecord::Migration
  def up
  	change_column :employees, :employee_code, :string
  end

  def down
  	change_column :employees, :employee_code, :integer
  end
end
