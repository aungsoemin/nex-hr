class AddRoleIdToEmpTbl5 < ActiveRecord::Migration
  
  def self.up 
  	add_column :employees, :employee_role_id, :integer
  end

  def self.down
  	remove_column :employees, :employee_role_id, :integer
  end
end
