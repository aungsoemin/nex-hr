class AddNrcBanckaccToEmpTbl4 < ActiveRecord::Migration
  
  def self.up
  	add_column :employees, :bank_account, :string
  	add_column :employees, :nrc, :string
  end

  def self.down
  	remove_column :employees, :bank_account, :string
  	remove_column :employees, :nrc, :string
  end
end
