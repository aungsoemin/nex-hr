class CreateHolidays < ActiveRecord::Migration
  def change
    create_table :holidays do |t|
      t.string :name
      t.integer :year
      t.date :date
      t.boolean :holiday_type
      t.integer :fix_holiday_id

      t.timestamps null: false
    end
  end
end
