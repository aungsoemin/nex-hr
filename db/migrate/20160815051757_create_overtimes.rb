class CreateOvertimes < ActiveRecord::Migration
  def change
    create_table :overtimes do |t|
    	t.date :requested_date
    	t.integer :employee_id
      t.datetime :start_date
      t.datetime :end_date
      t.float :ot_hours
      t.float :evaluated_hours
      t.string :project_name
      t.text :detail_tasks
      t.text :reason
      t.string :attachment
      t.integer :report_to_id
      t.integer :approved_by_id
      t.integer :status

      t.timestamps null: false
    end
  end
end
