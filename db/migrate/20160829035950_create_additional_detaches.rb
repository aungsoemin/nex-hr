class CreateAdditionalDetaches < ActiveRecord::Migration
  def change
    create_table :additional_detaches do |t|
    	t.string :name
    	t.integer :amount
    	t.date :detach_month

      t.timestamps null: false
    end
  end
end
