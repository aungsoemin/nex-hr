class RemoveSsbFromPayrollTbl1 < ActiveRecord::Migration
  
  def self.up
  	remove_column :payrolls, :ssb, :integer
  end

  def self.down
  	add_column :payrolls, :ssb, :integer
  end
end
