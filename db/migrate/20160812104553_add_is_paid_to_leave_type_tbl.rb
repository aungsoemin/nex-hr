class AddIsPaidToLeaveTypeTbl < ActiveRecord::Migration
  
  def self.up
  	add_column :leave_types, :is_paid, :boolean
  end

  def self.down
  	remove_column :leave_types, :is_paid, :boolean
  end
end
