class AddExperienceYearInLeaveTypeSettingTbl < ActiveRecord::Migration
  
  def self.up
  	add_column :leave_type_settings, :experience_year, :integer
  end

  def self.down
  	remove_column :leave_type_settings, :experience_year, :integer
  end
end
