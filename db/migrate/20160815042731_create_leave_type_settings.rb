class CreateLeaveTypeSettings < ActiveRecord::Migration
  def change
    create_table :leave_type_settings do |t|
    	t.string :name
    	t.integer :leave_type_id
    	t.integer :no_of_leave

      t.timestamps null: false
    end
  end
end
