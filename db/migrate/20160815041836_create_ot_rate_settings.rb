class CreateOtRateSettings < ActiveRecord::Migration
  def change
    create_table :ot_rate_settings do |t|
    	t.string :name
    	t.float :rate

      t.timestamps null: false
    end
  end
end
