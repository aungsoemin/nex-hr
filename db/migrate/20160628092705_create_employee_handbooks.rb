class CreateEmployeeHandbooks < ActiveRecord::Migration
  def change
    create_table :employee_handbooks do |t|
    	t.string :name 
    	t.string :file
    	t.boolean :is_published

      t.timestamps null: false
    end
  end
end
