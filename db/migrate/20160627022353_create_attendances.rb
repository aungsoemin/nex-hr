class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.integer :employee_id
      t.time :start_time
      t.time :end_time
      t.date :date

      t.timestamps null: false
    end
  end
end
