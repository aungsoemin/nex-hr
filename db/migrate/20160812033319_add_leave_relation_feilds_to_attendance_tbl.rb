class AddLeaveRelationFeildsToAttendanceTbl < ActiveRecord::Migration
  
  def self.up
  	add_column :attendances, :is_leave, :boolean
  	add_column :attendances, :leave_type_id, :integer
  end

  def self.down
  	remove_column :attendances, :is_leave, :boolean
  	remove_column :attendances, :leave_type_id, :integer
  end
end
