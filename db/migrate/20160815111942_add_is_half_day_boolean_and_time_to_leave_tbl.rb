class AddIsHalfDayBooleanAndTimeToLeaveTbl < ActiveRecord::Migration
  
  def self.up 
  	add_column :leaves, :is_half_day, :boolean 
  	add_column :leaves, :start_time, :time 
  	add_column :leaves, :end_time, :time
  end
  
  def self.down
  	remove_column :leaves, :is_half_day, :boolean 
  	remove_column :leaves, :start_time, :time 
  	remove_column :leaves, :end_time, :time
  end

end
