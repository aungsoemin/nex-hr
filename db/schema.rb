# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161005050534) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "additional_detaches", force: :cascade do |t|
    t.string   "name"
    t.integer  "amount"
    t.date     "detach_month"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "attendance_settings", force: :cascade do |t|
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "define_weekday"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "week_days"
  end

  create_table "attendance_sheets", force: :cascade do |t|
    t.integer  "employee_id"
    t.time     "start_time"
    t.time     "end_time"
    t.date     "date"
    t.integer  "submit_by_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "attendances", force: :cascade do |t|
    t.integer  "employee_id"
    t.time     "start_time"
    t.time     "end_time"
    t.date     "date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "is_leave"
    t.integer  "leave_type_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emergency_phones", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employee_handbooks", force: :cascade do |t|
    t.string   "name"
    t.string   "file"
    t.boolean  "is_published"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "employee_histories", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "title_id"
    t.integer  "department_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "employee_leave_counts", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "leave_type_id"
    t.float    "leave_count"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "employee_roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name",                   default: ""
    t.string   "employee_code",          default: "0"
    t.integer  "title_id",               default: 0
    t.integer  "department_id",          default: 0
    t.date     "dob"
    t.string   "address",                default: ""
    t.string   "profile_image",          default: ""
    t.date     "join_date"
    t.string   "phone",                  default: ""
    t.string   "personal_email",         default: ""
    t.string   "cv_file",                default: ""
    t.string   "performance_appraisal",  default: ""
    t.integer  "status",                 default: 0
    t.string   "auth_token",             default: ""
    t.string   "device_token",           default: ""
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "finger_print"
    t.integer  "basic_pay"
    t.integer  "gender"
    t.string   "bank_account"
    t.string   "nrc"
    t.integer  "employee_role_id"
  end

  add_index "employees", ["email"], name: "index_employees_on_email", unique: true, using: :btree
  add_index "employees", ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true, using: :btree

  create_table "fix_holidays", force: :cascade do |t|
    t.string   "name"
    t.date     "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "holidays", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.date     "date"
    t.boolean  "holiday_type"
    t.integer  "fix_holiday_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "leave_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leave_type_settings", force: :cascade do |t|
    t.string   "name"
    t.integer  "leave_type_id"
    t.integer  "no_of_leave"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "experience_year"
  end

  create_table "leave_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "is_paid"
  end

  create_table "leaves", force: :cascade do |t|
    t.date     "requested_date"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "leave_type_id"
    t.text     "reason"
    t.string   "attachment"
    t.integer  "employee_id"
    t.integer  "first_approved_by_id"
    t.integer  "second_approved_by_id"
    t.integer  "leave_status_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.boolean  "is_half_day"
    t.time     "start_time"
    t.time     "end_time"
  end

  create_table "ot_rate_settings", force: :cascade do |t|
    t.string   "name"
    t.float    "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "other_allowances", force: :cascade do |t|
    t.integer  "employee_id"
    t.string   "name"
    t.integer  "amount"
    t.date     "allowance_month"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "overtimes", force: :cascade do |t|
    t.date     "requested_date"
    t.integer  "employee_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.float    "ot_hours"
    t.float    "evaluated_hours"
    t.string   "project_name"
    t.text     "detail_tasks"
    t.text     "reason"
    t.string   "attachment"
    t.integer  "report_to_id"
    t.integer  "approved_by_id"
    t.integer  "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "payrolls", force: :cascade do |t|
    t.integer  "employee_id"
    t.date     "payroll_month"
    t.integer  "basic_pay"
    t.float    "overtime_duration"
    t.integer  "overtime_payment"
    t.integer  "bonus"
    t.integer  "commission"
    t.integer  "other_plus"
    t.float    "total_leave_count"
    t.float    "unpaid_leave_count"
    t.integer  "leave_deduction"
    t.float    "tax"
    t.integer  "other_detach"
    t.integer  "net_pay"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "taxes", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "amount"
    t.string   "tax_year"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "titles", force: :cascade do |t|
    t.string   "name"
    t.string   "rank"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
