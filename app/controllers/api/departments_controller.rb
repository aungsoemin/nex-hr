class Api::DepartmentsController < Api::BaseController

	def index
		@departments = Department.all
		respond_to do |format|
	      format.html
	      format.json{ render json: @departments, status: 200}
		end
	end
end
