class Api::BaseController < ActionController::Base
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token

  respond_to :json

  def check_auth_token
    if params[:auth_token].present?
      employee = Employee.where(auth_token: params[:auth_token]).first

      if employee.present?
        sign_in(employee, store: false)
      else
        return render json: { message: "Invalid Auth Token!" }, status: 401
      end
    else
      return render json: { message: "Require Auth Token!" }, status: 422
    end
  end
end
