class Api::EmployeeRolesController <  Api::BaseController

	def index
		@emp_roles = EmployeeRole.all
		respond_to do |format|
	      format.html
	      format.json{ render json: @emp_roles, status: 200}
		end
	end
	
end
