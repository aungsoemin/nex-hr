class Api::OvertimesController < Api::BaseController

	def index
		if params[:page].present?
			@overtimes = Overtime.all.joins(:employee,:employee => :department).paginate(per_page: 5, page: params[:page]).order("requested_date DESC, departments.name ASC, employees.name ASC")
		else
			@overtimes = Overtime.all
		end

		respond_to do |format|
			format.html
			format.json{ render json: @overtimes, status: 200}
		end
	end

	def create
		@overtime = Overtime.new(permit_params)
		if @overtime.save
			respond_to do |format|
	      format.html
	      format.json{ render json: @overtime, status: 200}
			end
		else
			return render json: { message: @overtime.errors}, status: 404
		end
	end

	def show
		@overtime = Overtime.where(id: params[:id]).first
		return render json: { message: 'Invalid Overtime'}, status: 400 if @overtime.blank?
		
		respond_to do |format|
      format.html
      format.json{ render json: @overtime, status: 200}
		end
	end

	def update
		@overtime = Overtime.where(id: params[:id]).first
		return render json: { message: 'Invalid Leave'}, status: 400 if @overtime.blank?
		if @overtime.update(permit_params)
			respond_to do |format|
	      format.html
	      format.json{ render json: @overtime, status: 200}
			end
		else
			return render json: { message: @overtime.errors}, status: 404
		end
	end

	private
	def permit_params
    params.permit(:requested_date, :employee_id, :start_date, :end_date, :ot_hours, :evaluated_hours, :project_name, :detail_tasks, :reason, :attachment, :report_to_id, :approved_by_id, :status)
  end
end
