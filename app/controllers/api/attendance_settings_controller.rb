class Api::AttendanceSettingsController < Api::BaseController
	
	def index
		if params[:page].present?
			@attendance_settings = AttendanceSetting.all.paginate(per_page: 5, page: params[:page])
		else
			@attendance_settings = AttendanceSetting.all
		end
		@setting_hash = Hash.new
		@attendance_settings.each do |a|
			week_days = a.week_days.scan(/(?<=\[)[^\]]+?(?=\])/).last
			days = week_days.split(',')
			@weekdays = []
			days.each do |d|
				@weekdays << d.to_i
			end
			@setting_hash["#{a.id}"] = @weekdays
		end
	end

	def show
		@attendance_setting = AttendanceSetting.where(:id => params[:id]).first
		return render json: { message: "Invalid Attendance Setting!"} if @attendance_setting.blank?
		week_days = @attendance_setting.week_days.scan(/(?<=\[)[^\]]+?(?=\])/).last
		days = week_days.split(',')
		@weekdays = []
		days.each do |d|
			@weekdays << d.to_i
		end
	end

end

