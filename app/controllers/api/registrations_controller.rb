class Api::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token
	respond_to :json

	def create
		return render json: { message: "Require email"}, status: 400 if params[:email].blank?
		if params[:email].downcase == "test@test.com"
      return render json: {message: "#{params[:test]} is not a valid email address"}
    end
    return render json: { message: "Require password"}, status: 400 if params[:password].blank?

    @employee = Employee.where(:email => params[:email]).first
    if @employee.present?
      return render json: { message: "Employee with email address already exist!"}, status: 400
    else
      @employee = Employee.new(permit_params)

        profile = params[:profile_image]
        if profile.present?

          data_url = profile
          data_type = data_url.split(',')

            # Check which decode method to use
          if ( data_type[0] == 'data:image/jpeg;base64' )
            png = Base64.decode64(data_url['data:image/jpeg;base64,'.length .. -1])
          elsif ( data_type[0] == 'data:image/png;base64' )
            png = Base64.decode64(data_url['data:image/png;base64,'.length .. -1])
          else
            return render json: { message: "image type not supported" }, status: 400
          end

          tmp_file_name = Time.now.to_s << 'profile-image.png'

          File.open('public/' + tmp_file_name, 'wb') { |f| f.write(png) }

          image = MiniMagick::Image.open('public/'+ tmp_file_name)

          @path = "public/"

          @file_name = 'thumb'

          image.write(@path + "#{@file_name}.jpg")

          @data = File.open("#{@path}#{@file_name}.jpg", "rb")
         
          @employee.profile_image = @data

          # delete the temp files
          if ( @data )
          File.delete(Rails.root + "#{@path}#{@file_name}.jpg")
          File.delete(Rails.root + "#{@path}#{tmp_file_name}") if tmp_file_name.present?
          end
        end
        
      # end
      @employee.generate_authentication_token
      if @employee.save
        respond_to do |format|
          format.html
          format.json{ render json: @employee, status: 200}
        end
      else
        return render json: { message: @employee.errors}, status: 404
      end
    end
	end

	private
  def permit_params
    params.permit(:employee_role_id, :name, :email, :password, :title_id, :department_id, :profile_image, :bank_account, :nrc, :dob, :address, :phone, :gender, :personal_email, :join_date, :cv_file, :performance_appraisal, :basic_pay, :status )
  end
end