class Api::OtRateSettingsController < Api::BaseController

	def index
		@ot_rate_settings = OtRateSetting.all
	end

	def show
		@ot_rate_setting = OtRateSetting.where(:id => params[:id]).last
		return render json: { message: "Invalid Leave Type!"} if @ot_rate_setting.blank?
	end

	def update
		@ot_rate_setting = OtRateSetting.where(:id => params[:id]).first
		return render json: { message: "Invalid Leave Type!"} if @ot_rate_setting.blank?

		unless @ot_rate_setting.update(permit_params)
			return render json: { message: @ot_rate_setting.errors}, status: 404
		end
	end

	private
	def permit_params
    params.permit(:name, :rate)
  end

end
