class Api::PayrollsController < Api::BaseController

	def index
		return render json: { message: 'Require Month!'}, status: 400 if params[:month].blank?
		return render json: { message: 'Require Year!'}, status: 400 if params[:year].blank?
		@month_year = "#{params[:month]}-#{params[:year]}"

		if params[:page].present?
			@payrolls = Payroll.where("extract(month from payroll_month) = ? AND extract(year from payroll_month) = ?",params[:month],params[:year]).joins(:employee,:employee => :department).paginate(per_page: 5, page: params[:page]).order("updated_at DESC, departments.name ASC, employees.name ASC")
		else
			@payrolls = Payroll.where("extract(month from payroll_month) = ? AND extract(year from payroll_month) = ?",params[:month],params[:year])
		end
		@last_generated_date = Payroll.all.order("updated_at").last
	end

	def payroll
		other_detach = AdditionalDetach.other_detach(params[:month],params[:year])
		d = Date.parse("#{params[:year]}-#{params[:month]}-01")
		working_days,working_hours =  AttendanceSetting.working_days(d)
		# production
		holiday = Holiday.where("extract(month from date) = ? AND year = ?",params[:month],params[:year])
		# development
		# holiday = Holiday.where("strftime('%m',date) = ? AND year = ?","#{params[:month]}",params[:year])
		holiday_arr = []
		holiday.each do |h|
			holiday_arr << h.date.strftime("2016-%m-%d")
		end

		@emp_payrolls = []

		@employees = Employee.where(:status => 1)
		@employees.each do |employee|
			basic_pay = employee.basic_pay
			yearly_basic_pay = basic_pay.to_i * 12 
			hourly_rate = ((yearly_basic_pay.to_i / working_days.to_i ) / working_hours.to_i).round
			puts "Hourly Rate =====> #{hourly_rate}"

			overtime_hour,overtime_payment = Overtime.overtime_pay(holiday_arr,employee,params[:month],params[:year],hourly_rate)

			other_allowance = OtherAllowance.total_allowance(employee.id,params[:month],params[:year])
			all_addition = overtime_payment.to_i + other_allowance.to_i

			all_leave_count,unpay_leave_count,deduction = Leave.leave_count_deduction(employee,params[:month],params[:year],hourly_rate)
			monthly_tax = Tax.tax_amount(employee.id,params[:year])
			all_deduction = deduction.to_i + monthly_tax.to_i + other_detach.to_i

			payroll_now_month = Date.parse("#{params[:year]}-#{params[:month]}-01").at_end_of_month
			net_pay = (basic_pay.to_i + all_addition.to_i) - all_deduction.to_i

			# production
			@payroll = Payroll.where("employee_id = ? AND extract(month from payroll_month) = ? AND extract(year from payroll_month) = ?",employee.id, params[:month],params[:year]).first
			# development
			# @payroll = Payroll.where("employee_id = ? AND strftime('%m',payroll_month) = ? AND strftime('%Y',payroll_month) = ?",employee.id, "#{params[:month]}",params[:year]).first
			if @payroll.present?
				@payroll.payroll_month = payroll_now_month
				@payroll.basic_pay = basic_pay
				@payroll.overtime_duration = overtime_hour
				@payroll.overtime_payment = overtime_payment
				@payroll.other_plus = other_allowance
				@payroll.total_leave_count = all_leave_count
				@payroll.unpaid_leave_count = unpay_leave_count
				@payroll.leave_deduction = deduction
				@payroll.tax = monthly_tax
				@payroll.other_detach = other_detach
				@payroll.net_pay = net_pay
				@payroll.save
			else
				@payroll = Payroll.new()
				@payroll.employee_id = employee.id
				@payroll.payroll_month = payroll_now_month
				@payroll.basic_pay = basic_pay
				@payroll.overtime_duration = overtime_hour
				@payroll.overtime_payment = overtime_payment
				@payroll.bonus = 0
				@payroll.commission = 0
				@payroll.other_plus = other_allowance
				@payroll.total_leave_count = all_leave_count
				@payroll.unpaid_leave_count = unpay_leave_count
				@payroll.leave_deduction = deduction
				@payroll.tax = monthly_tax
				@payroll.other_detach = other_detach
				@payroll.net_pay = net_pay
				@payroll.save
			end	
			@emp_payrolls << @payroll
		end
	end

	def detail_payroll
		employee = Employee.where(:id => params[:employee_id]).first
		return render json: { message: "Invalid Employee!" },status: 400 if employee.blank?
		if params[:month].blank? || params[:year].blank?
			return render json: { message: "Please Specify Date!"},status: 400
		end
		# production
		@detail = Payroll.where("employee_id = ? AND extract(month from payroll_month) = ? AND extract(year from payroll_month) = ?",params[:employee_id].to_i,params[:month].to_i,params[:year].to_i).first
		#development
		# @detail = Payroll.where("employee_id = ? AND strftime('%m',payroll_month) = ? AND strftime('%Y',payroll_month) = ?",params[:employee_id],"#{params[:month]}",params[:year]).first
		# production
		@leaves = Leave.where("employee_id = ? AND ((extract(month from start_date) =? AND extract(year from start_date) =?) OR (extract(month from end_date) = ? AND extract(year from end_date) = ?))",params[:employee_id],params[:month],params[:year],params[:month],params[:year])
		# development
		# @leaves = Leave.where("employee_id = ? AND ((strftime('%m',start_date) = ? AND strftime('%Y',start_date) = ?) OR (strftime('%m',end_date) = ? AND strftime('%Y',end_date) = ?))",params[:employee_id],"#{params[:month]}",params[:year],"#{params[:month]}",params[:year])
		# production
		@overtimes = Overtime.where("employee_id = ? AND status = ? AND ((extract(month from start_date) = ? AND extract(year from start_date) = ?) OR (extract(month from end_date) = ? AND extract(year from end_date) = ?))",params[:employee_id],2,params[:month],params[:year],params[:month],params[:year])
		# development
		# @overtimes = Overtime.where("employee_id = ? AND status = ? AND ((strftime('%m',start_date) = ? AND strftime('%Y',start_date) = ?) OR (strftime('%m',end_date)= ? AND strftime('%Y',end_date)= ?))",params[:employee_id],2,"#{params[:month]}",params[:year],"#{params[:month]}",params[:year])
		# production
		@additional_detaches = AdditionalDetach.where("extract(month from detach_month) = ? AND extract(year from detach_month) = ? ",params[:month],params[:year])
		# development
		# @additional_detaches = AdditionalDetach.where("strftime('%m',detach_month) = ? AND strftime('%Y',detach_month) = ? ","#{params[:month]}",params[:year])
		# production
		@other_allowances = OtherAllowance.where("employee_id = ? AND extract(month from allowance_month) = ? AND extract(year from allowance_month) = ?",params[:employee_id],params[:month],params[:year])
		# development
		# @other_allowances = OtherAllowance.where("employee_id = ? AND strftime('%m',allowance_month) = ? AND strftime('%Y',allowance_month) = ?",params[:employee_id],"#{params[:month]}",params[:year])
		return render json: { message: "Payroll doesn't exist!"}, status: 204 if @detail.blank?
	end  
end
