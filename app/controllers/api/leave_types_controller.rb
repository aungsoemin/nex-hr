class Api::LeaveTypesController < Api::BaseController
	def index
		@leave_types = LeaveType.all

		respond_to do |format|
			format.html
			format.json{ render json: @leave_types, status: 200}
		end
	end

	def create
		@leave_type = LeaveType.new(permit_params)
		unless @leave_type.save
			return render json: { message: @leave_type.errors}, status: 404
		end
	end

	def show
		@leave_type = LeaveType.where(:id => params[:id]).first
		return render json: { message: "Invalid Leave Type!"} if @leave_type.blank?

		respond_to do |format|
			format.html
			format.json{ render json: @leave_type, status: 200}
		end
	end

	def update
		@leave_type = LeaveType.where(:id => params[:id]).first
		return render json: { message: "Invalid Leave Type!"} if @leave_type.blank?

		unless @leave_type.update(permit_params)
			return render json: { message: @leave_type.errors}, status: 404
		end
	end

	private
	def permit_params
    params.permit(:name, :is_paid)
  end
end
