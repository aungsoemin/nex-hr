class Api::HolidaysController < Api::BaseController
	def index
		@holidays = Holiday.where(:year => params[:year]).order("date asc")
	end
end
