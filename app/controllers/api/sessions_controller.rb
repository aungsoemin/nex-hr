class Api::SessionsController < Devise::SessionsController
  # respond_to :json
  skip_before_filter :verify_authenticity_token, :require_no_authentication
  before_filter :check_auth_token, only: [:sign_out]
  skip_filter :verify_signed_out_employee, only: :destroy

  # login
  def create
    return render json: { message: "Require email"}, status: 400 if params[:email].blank?
    return render json: { message: "Require password"}, status: 400 if params[:password].blank?

    employee = Employee.find_by(email: params[:email])

    if employee.present?
      if employee.valid_password? params[:password]
        employee.generate_authentication_token
        employee.save!
        sign_in(:employee, employee)

        @employee = current_employee
        respond_to do |format|
          format.html
          format.json { render json: @employee, status: 200}
        end
      else
        return render status: 400, json: { message: "Email and password does not match." }
      end
    else
      render status: 400, json: { message: "Invalid account." }
    end
  end

  #logout
  def destroy
    return render json: { message: "Require token"},status: 400 if params[:auth_token].blank?
    employee = Employee.where(auth_token: params[:auth_token]).first
    if employee.present?
      employee.auth_token = ''
      employee.save!
      return render json: { message: "Logout successfully!"}, stauts: 200
    else
      render status: 400, json: { message: "Invalid token." }
    end
  end

  #forgot password
  def forgot_password
    return render json: { message: "Require email"}, status: 400 if params[:email].blank?
    employee = Employee.find_by(email: params[:email])
    if employee.present?
      employee.send_reset_password_instructions
      render status: 200, json: { message: "Email for reset password has been sent"}
    else
      render status: 400, json: { message: "Invalid email"}
    end
  end

end
