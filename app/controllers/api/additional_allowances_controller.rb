class Api::AdditionalAllowancesController < Api::BaseController
	
	def index
		if params[:page].present?
			@additional_allowances = OtherAllowance.all.joins(:employee).paginate(per_page: 5, page: params[:page]).order("allowance_month DESC, employees.name ASC, name ASC")
		else
			@additional_allowances = OtherAllowance.all.joins(:employee).order("allowance_month DESC, employees.name ASC, name ASC")
		end

		respond_to do |format|
			format.html
			format.json{ render json: @additional_allowances, status: 200}
		end
	end

	def destroy
		@additional_allowance = OtherAllowance.where(:id => params[:id]).first
		return render json: { message: "Invalid Additional Allowance!"} if @additional_allowance.blank?

		if @additional_allowance.destroy
			return render json: { message: "Deleted Successfully!"}, status: 200
		else
			return render json: { message: @additional_allowance.errors} ,status: 404
		end
	end

end
