class Api::LeaveTypeSettingsController < Api::BaseController

	def index
		@leave_type_settings = LeaveTypeSetting.all
	end

	def show
		@leave_type_setting = LeaveTypeSetting.where(:id => params[:id]).first
		return render json: { message: "Invalid Leave Type!"} if @leave_type_setting.blank?
	end

	def update
		@leave_type_setting = LeaveTypeSetting.where(:id => params[:id]).first
		return render json: { message: "Invalid Leave Type!"} if @leave_type_setting.blank?

		unless @leave_type_setting.update(permit_params)
			return render json: { message: @leave_type_setting.errors}, status: 404
		end
	end

	private
	def permit_params
    params.permit(:name, :leave_type_id, :no_of_leave, :experience_year)
  end

end
