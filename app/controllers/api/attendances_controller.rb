class Api::AttendancesController < Api::BaseController
	def index
		if params[:page].present?
			@attendances = Attendance.all.joins(:employee,:employee => :department).paginate(per_page: 5, page: params[:page]).order("date DESC, departments.name ASC, employees.name ASC")
		else
			@attendances = Attendance.all
		end

		respond_to do |format|
			format.html
			format.json{ render json: @attendances, status: 200}
		end
	end

	def create 
		@attendance = Attendance.new(permit_params)
		if @attendance.save
			respond_to do |format|
	      format.html
	      format.json{ render json: @attendance, status: 200}
			end
		else
			return render json: { message: @attendance.errors}, status: 404
		end
	end

	def emp_attendance
		if params[:month].present?
			# by whole month
			@attendances = Attendance.where(:employee_id => params[:employee_id]).where("extract(year from date) = '?' AND extract(month from date) = ?" , params[:month].to_date.year, params[:month].to_date.strftime('%m')).order(:date)
		elsif params[:start_date].present? && params[:end_date].present?
			# by date_range
			@attendances = Attendance.where(:employee_id => params[:employee_id], :date => params[:start_date].to_date..params[:end_date].to_date).order(:date)
		else
			return render json: {message: "Enter starrt_date/end_date or month."}, :status => 400
		end
		
		respond_to do |format|
	      format.html
	      format.json{ render json: @attendances, status: 200}
		end
	end

end

private
def permit_params
  params.permit(:employee_id, :start_time, :end_time, :date, :is_leave, :leave_type_id)
end

# development {sqlite}
# .where("strftime('%m-%Y', date) = ? ", params[:month].to_date.strftime('%m-%Y'))    
# production {postgres}
# .where("extract(year from date) = '?' AND extract(month from date) = ?" , params[:month].to_date.year, params[:month].to_date.strftime('%m'))