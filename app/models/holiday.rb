class Holiday < ActiveRecord::Base
	belongs_to :fix_holiday
	
	before_save :save_date
	def save_date
		if self.holiday_type? && self.fix_holiday_id.present?
			@fix_holiday = FixHoliday.find(self.fix_holiday_id)
			self.name = @fix_holiday.present? ? @fix_holiday.name : ''
			self.date = @fix_holiday.present? ? @fix_holiday.date : ''
		elsif !self.holiday_type?
			self.fix_holiday_id = ''
		end
	end
end
