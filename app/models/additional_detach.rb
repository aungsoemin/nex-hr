class AdditionalDetach < ActiveRecord::Base

	def self.other_detach(month,year)
		# production
		@detach = AdditionalDetach.where("extract(month from detach_month) = ? AND extract(year from detach_month) = ? ",month,year)
		# development
		# @detach = AdditionalDetach.where("strftime('%m',detach_month) = ? AND strftime('%Y',detach_month) = ? ","#{month}",year)
		if @detach.present?
			detach_amount = @detach.sum(:amount)
			return detach_amount
		else 
			return 0
		end
	end
end
