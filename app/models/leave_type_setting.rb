class LeaveTypeSetting < ActiveRecord::Base
	belongs_to :leave_type

	validates_presence_of :name, :leave_type_id, :experience_year, :no_of_leave
end
