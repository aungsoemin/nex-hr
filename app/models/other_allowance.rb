class OtherAllowance < ActiveRecord::Base
	belongs_to :employee

	def self.total_allowance(id,month,year)
		# production
		@allowance = OtherAllowance.where("employee_id = ? AND extract(month from allowance_month) = ? AND extract(year from allowance_month) = ?",id,month,year)
		# development
		# @allowance = OtherAllowance.where("employee_id = ? AND strftime('%m',allowance_month) = ? AND strftime('%Y',allowance_month) = ?",id,"#{month}",year)
		total_allowance = 0
		if @allowance.present?
			total_allowance = @allowance.sum(:amount)
		end
		return total_allowance
	end
end
