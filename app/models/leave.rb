class Leave < ActiveRecord::Base
	belongs_to :leave_type
	belongs_to :leave_status
	belongs_to :employee
	belongs_to :first_approved_by, class_name: "Employee", primary_key: "id"
	belongs_to :second_approved_by, class_name: "Employee", primary_key: "id"
	
	mount_uploader :attachment, FileUploader

	validates_presence_of :employee_id, :requested_date, :start_date, :end_date, :leave_type_id, :reason

	before_create :save_status
	after_save :save_time

	def save_status
		pending = LeaveStatus.where(:id => 1 ).first
		self.leave_status_id = 1 if self.leave_status_id.blank?
	end

	def save_time
		if self.changed_attributes["is_half_day"].present?
			leave = Leave.where(:id => self.id).last
			unless leave.is_half_day
				leave.start_time =""
				leave.end_time =""
				leave.save
				puts "last_record : #{leave.id}"
			end
		end

		# development
		# emp_leave_count = EmployeeLeaveCount.where("employee_id = ? AND leave_type_id = ? ", self.employee_id, self.leave_type_id).first
		# production
		emp_leave_count = EmployeeLeaveCount.where("employee_id::integer = ? AND leave_type_id::integer = ? ", self.employee_id, self.leave_type_id).first
		no_of_days = (self.end_date - self.start_date).to_i + 1
		# find and update employee leave count
		if self.is_half_day
			if emp_leave_count.present?
				emp_leave_count.leave_count += 0.5
				emp_leave_count.save
			else
				emp_leave = EmployeeLeaveCount.new()
	      emp_leave.employee_id = self.employee_id
	      emp_leave.leave_type_id = self.leave_type_id
	      emp_leave.leave_count = 0.5
	      emp_leave.save
			end
		else
			if emp_leave_count.present?
				emp_leave_count.leave_count += no_of_days
				emp_leave_count.save
			else
				emp_leave = EmployeeLeaveCount.new()
	      emp_leave.employee_id = self.employee_id
	      emp_leave.leave_type_id = self.leave_type_id
	      emp_leave.leave_count = no_of_days
	      emp_leave.save
			end
		end
	end

	def self.leave_count_deduction(employee,month,year,hourly_rate)
		basic_pay = employee.basic_pay
		unpaid_leave_types = LeaveType.where(:is_paid => false).all.map{|m| m.id}
		# production
		@all_leaves = Leave.where("employee_id = ? AND leave_status_id = ? AND extract(month from start_date) = ? AND extract(year from start_date) = ? AND extract(month from end_date) = ? AND extract(year from end_date) = ?",employee.id,2,month,year,month,year)
		# development
		# @all_leaves = Leave.where("employee_id = ? AND leave_status_id = ? AND ((strftime('%m',start_date) = ? AND strftime('%Y',start_date) = ?) OR (strftime('%m',end_date) = ? AND strftime('%Y',end_date) = ?))",employee.id,2,"#{month}",year,"#{month}",year)
		# get all the leaves within month
		@leave_duration = 0
		@unpay_leaves_count = 0

		if @all_leaves.present?
			@all_leaves_count = @all_leaves.where(:is_half_day => false).count.to_i + @all_leaves.where(:is_half_day => true).count.to_f / 2
		
			# full unpaid leaves duration in hours
			full_unpaid_leaves = @all_leaves.where("leave_type_id IN(?) AND is_half_day = ?",unpaid_leave_types,false)
			full_unpaid_leaves_count = 0
			full_unpaid_leaves.each do |full_unpaid|
				full_unpaid_leaves_count += (full_unpaid.end_date - full_unpaid.start_date).to_i + 1			
			end
			attendance_setting = AttendanceSetting.last
			@leave_duration += full_unpaid_leaves_count.to_i * ((attendance_setting.end_time - attendance_setting.start_time) / 1.hour).round

			# get halfday unpaid leaves duration in hours
			half_unpaid_leaves = @all_leaves.where("leave_type_id IN(?) AND is_half_day = ?",unpaid_leave_types,true)
			half_unpaid_leaves_count = half_unpaid_leaves.count.to_f / 2 
			half_unpaid_leaves.each do |half_unpaid|
				@leave_duration += ((half_unpaid.end_time - half_unpaid.start_time) / 1.hour).round
			end
			@unpay_leaves_count = full_unpaid_leaves_count.to_i + half_unpaid_leaves_count.to_f
		else
			@all_leaves_count = 0
		end
		
		deduction = @leave_duration.to_i * hourly_rate.to_i

		return @all_leaves_count,@unpay_leaves_count,deduction
	end

end
