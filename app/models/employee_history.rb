class EmployeeHistory < ActiveRecord::Base
	belongs_to :department
	belongs_to :title
	belongs_to :employee
end
