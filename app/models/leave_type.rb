class LeaveType < ActiveRecord::Base
	has_many :leaves
	has_many :attendances
	has_many :leave_type_settings
	has_many :employee_leave_counts, :dependent => :destroy
end
