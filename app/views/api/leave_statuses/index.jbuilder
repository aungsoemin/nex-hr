json.array! @leave_statuses do |status|
	json.id status.id.present? ? status.id : ''
	json.name status.name.present? ? status.name : ''
end