empty = {}
json.array! @leave_type_settings do |setting|
	json.id setting.id
	json.name setting.name.present? ? setting.name : ''
	if setting.leave_type.present?
  	json.leave_type setting.leave_type, :id, :name
  else
  	json.leave_type empty
  end
  json.no_of_leave setting.no_of_leave.present? ? setting.no_of_leave : ''
  json.experience_year setting.experience_year.present? ? setting.experience_year : ''
end