json.id @leave_type_setting.id
	json.name @leave_type_setting.name.present? ? @leave_type_setting.name : ''
	if @leave_type_setting.leave_type.present?
  	json.leave_type @leave_type_setting.leave_type, :id, :name
  else
  	json.leave_type empty
  end
  json.no_of_leave @leave_type_setting.no_of_leave.present? ? @leave_type_setting.no_of_leave : ''
  json.experience_year @leave_type_setting.experience_year.present? ? @leave_type_setting.experience_year : ''