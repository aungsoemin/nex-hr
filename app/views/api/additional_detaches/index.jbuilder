json.array! @additional_detaches do |a_d|
	json.id a_d.id
	json.name a_d.name
	json.detach_month a_d.detach_month
	json.amount a_d.amount
	json.month a_d.detach_month.strftime('%-m')
	json.year a_d.detach_month.strftime('%Y')
end