empty = {}
json.array! @employees do |e|
	json.id e.id
	json.name e.name.present? ? e.name : ''
	json.email e.email.present? ? e.email : ''
	json.employee_code e.employee_code.present? ? e.employee_code : ''

	if e.employee_role.present?
		json.employee_role e.employee_role, :id, :name
	else
		json.employee_role empty
	end

	if e.title.present?
		json.title e.title, :id, :name, :rank
	else
		json.title empty
	end

	if e.department.present?
		json.department e.department, :id, :name, :code
	else
		json.department empty
	end

	json.bank_account e.bank_account? ? e.bank_account : ''
	json.nrc e.nrc? ? e.nrc : ''
	json.dob e.dob? ? e.dob.strftime("%Y-%m-%d") : ''
	json.address e.address? ? e.address : ''
	json.profile_image_photo_original_url e.profile_image.present? ? e.profile_image.url : ''
	json.profile_image_thumb_url e.profile_image.present? ? e.profile_image.thumb.url : ''
	json.profile_image_thumbsamll_url e.profile_image.present? ? e.profile_image.thumbsmall.url : ''
	json.join_date e.join_date? ? e.join_date.strftime("%Y-%m-%d") : ''

	@phones = []
	@phones = e.phone? ? e.phone.split(",") : ''
	json.phone @phones
	
	json.personal_email e.personal_email? ? e.personal_email : ''
	json.cv_file e.cv_file? ? e.cv_file.url : ''
	json.performance_appraisal e.performance_appraisal.present? ? e.performance_appraisal : ''
	json.basic_pay e.basic_pay.present? ? e.basic_pay.to_s : ''
	json.gender e.gender.present? ? e.gender : ''

	if e.join_date.present?
		today = Date.today
		 
		date1 = today.year*12 + today.month
		date2 = e.join_date.year.to_i*12 + e.join_date.month.to_i
		month = date1 - date2

		@ser_length = month.divmod(12)
	else
		@ser_length = []
	end
	json.ser_length @ser_length

	json.status e.status? ? e.status : ''
	json.device_token e.device_token? ? e.device_token : ''
	json.finger_print e.finger_print? ? e.finger_print : ''
	json.created_at e.created_at? ? e.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.updated_at e.updated_at? ? e.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ''

	json.emergency_phones do
	  json.array! e.emergency_phones do |e_p|
	    json.name e_p.name.present? ? e_p.name : ''
	    json.phone e_p.phone.present? ? e_p.phone : ''
	  end
	end
end