empty = {}
json.array! @history do |history|
	json.id history.id
	
	if history.employee.present?
		json.employee history.employee, :id, :name
	else
		json.employee empty
	end

	if history.title.present?
		json.title history.title, :id, :name
	else
		json.title empty
	end

	if history.department.present?
		json.department history.department, :id, :name
	else
		json.department empty
	end

	json.start_date history.start_date.present? ? history.start_date.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.end_date history.end_date.present? ? history.end_date.strftime("%Y-%m-%d %H:%M:%S") : ''
end