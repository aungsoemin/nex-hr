empty = {}
json.array! @overtimes do |overtime|
	json.id overtime.id
	json.requested_date overtime.requested_date.present? ? overtime.requested_date.strftime("%Y-%m-%d") : ''
	
	if overtime.employee.present?
		json.employee overtime.employee, :id, :name
	else
		json.report_to_id empty
	end

	if overtime.employee.department.present?
		json.department overtime.employee.department, :id,:name
	else
		json.department empty
	end
	json.date overtime.start_date.present? ? overtime.start_date.strftime("%d-%b") : ''
	json.start_time overtime.start_date.present? ? overtime.start_date.strftime("%I:%M %p") : ''
	json.end_time overtime.end_date.present? ? overtime.end_date.strftime("%I:%M %p") : ''
	json.ot_hours overtime.ot_hours.present? ? overtime.ot_hours.to_s : ''
	json.evaluated_hours overtime.evaluated_hours.present? ? overtime.evaluated_hours.to_s : ''
	
	json.status overtime.status.present? ? overtime.status : ''
end