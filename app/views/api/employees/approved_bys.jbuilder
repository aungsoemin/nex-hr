empty = {}
json.array! @approved_bys do |a|
	json.id a.id
	json.name a.name
	if a.employee_role.present?
		json.role a.employee_role, :id, :name
	else
		json.role {}
	end
	
	if a.department.present?
		json.department a.department, :id, :name
	else
		json.department empty
	end
	
end