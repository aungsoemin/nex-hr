empty = {}
json.array! @ot_rate_settings do |setting|
	json.id setting.id
	json.name setting.name.present? ? setting.name : ''
  json.rate setting.rate.present? ? setting.rate : ''
end