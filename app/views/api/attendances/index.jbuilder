empty = {}
json.array! @attendances do |attendance|
	json.id attendance.id
	if attendance.employee.present?
		json.employee attendance.employee, :id, :name
	end
	if attendance.employee.blank?
		json.employee empty
	end
	
	if attendance.employee.department.present?
		json.department attendance.employee.department, :id,:name
	else
		json.department attendance.employee.department
	end

	json.start_time attendance.start_time.present? ? attendance.start_time.strftime("%H:%M:%S") : ''
	json.end_time attendance.end_time.present? ? attendance.end_time.strftime("%H:%M:%S") : ''
	json.date attendance.date.present? ? attendance.date.strftime("%Y-%m-%d") : ''
	json.is_leave attendance.is_leave.present? ? attendance.is_leave : false 
	json.created_at attendance.created_at.present? ? attendance.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.updated_at attendance.updated_at.present? ? attendance.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ''

	if attendance.start_time.present? && attendance.end_time.present?
		@time = TimeDifference.between(attendance.start_time, attendance.end_time).in_general
		hour = @time[:hours]
		minute = @time[:minutes]
		seconds = @time[:seconds]
		json.total_hour  hour.to_s + ":" + minute.to_s + ":" + seconds.to_s
	else
		json.total_hour ""
	end
end