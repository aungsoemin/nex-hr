empty = {}
json.array! @attendances do |attendance|
	json.id attendance.id
	
	if attendance.employee.present?
		json.employee attendance.employee, :id, :name
	end
	if attendance.employee.blank?
		json.employee empty
	end
	
	json.start_time attendance.start_time.present? ? attendance.start_time.strftime("%H:%M:%S") : ''
	json.end_time attendance.end_time.present? ? attendance.end_time.strftime("%H:%M:%S") : ''
	json.date attendance.date.present? ? attendance.date.strftime("%Y-%m-%d") : ''
	json.created_at attendance.created_at.present? ? attendance.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.updated_at attendance.updated_at.present? ? attendance.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ''
end