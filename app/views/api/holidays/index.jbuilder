json.array! @holidays do |holiday|
	json.name holiday.name.present? ? holiday.name : ''
	json.date holiday.date.present? ? holiday.date.strftime("%b, %d") : ''
	json.year holiday.year.present? ? holiday.year : ''
end