empty = {}
json.array! @additional_allowances do |a|
	json.id a.id

	if a.employee.present?
		json.employee a.employee, :id, :name
	else
		json.employee empty
	end

	json.name a.name
	json.allowance_month a.allowance_month
	json.amount a.amount
	json.month a.allowance_month.strftime('%-m')
	json.year a.allowance_month.strftime('%Y')
end