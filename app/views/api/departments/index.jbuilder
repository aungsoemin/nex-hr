json.array! @departments do |d|
 json.id d.id
 json.name d.name.present? ? d.name : ''
 json.code d.code.present? ? d.code : ''
end