class ApplicationMailer < ActionMailer::Base
	default from: "<no-reply@nex-hr.example.com>"
	layout 'mailer'
end