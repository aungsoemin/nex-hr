ActiveAdmin.register Payroll do

	menu parent: 'Payroll'

	actions :index, :show

	permit_params :employee_id, 
	:payroll_month, 
	:basic_pay, 
	:overtime_duration, 
	:overtime_payment, 
	:bonus,
	:commission, 
	:other_plus, 
	:total_leave_count, 
	:unpaid_leave_count, 
	:leave_deduction,
	:tax,
	:ssb,
	:other_detach,
	:net_pay

	filter :employee
	filter :payroll_month
	filter :basic_pay
	filter :net_pay

	index do
	  selectable_column
	  id_column
	  column :employee
	  column :payroll_month, :label => "Month"
	  column :basic_pay
	  column :net_pay
	  actions
	end

	show do |i|
	  attributes_table do
	    row :id
	    row :employee_id
	    row :payroll_month
			row :basic_pay
			row :overtime_duration
			row :overtime_payment
			row :bonus
			row :commission
			row :other_plus
			row :total_leave_count
			row :unpaid_leave_count
			row :leave_deduction
			row :tax
			row :ssb
			row :other_detach
			row :net_pay
	  end
	end

end
