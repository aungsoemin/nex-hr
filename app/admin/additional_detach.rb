ActiveAdmin.register AdditionalDetach do

	menu parent: 'Payroll'

	permit_params :name, :amount, :detach_month

	form do |f|
    f.inputs "New Additional Detach" do
		  f.input :name
		  f.input :amount
		  f.input :detach_month,:as => :datepicker
    end
    f.actions
	end

end
