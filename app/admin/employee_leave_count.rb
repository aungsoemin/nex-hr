ActiveAdmin.register EmployeeLeaveCount do

	menu parent: 'Leave'
	actions :index

	permit_params :employee_id, :leave_type_id, :leave_count

	index do 
		selectable_column
    id_column
    column :employee
    column :leave_type
    column :leave_count
    actions
	end

end
