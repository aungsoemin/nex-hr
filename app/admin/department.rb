ActiveAdmin.register Department do
config.batch_actions = false
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  menu parent: 'Employee'

  permit_params :name, :code
    
  index do
      selectable_column
      id_column
      column :name
      column :code
      actions
   end

  filter :name
  filter :code

  form do |f|
    f.inputs "New Department" do
      f.input :name
      f.input :code
    end
    f.actions
  end
 
  show do
    attributes_table do
      row :id
      row :name
      row :code
      row :created_at
      row :updated_at
    end
  end

  controller do 
    def destroy
      employees = Employee.where("department_id = ? ","#{resource.id}").count
      puts employees
      if employees > 0
        flash[:error] = "You can't delete. #{employees} Employee(s) with the Department ID - #{resource.id} exist! "
        redirect_to :back
      else
        Department.destroy_all(id:"#{resource.id}")
        flash[:notice] = "Delete Successful! "
        redirect_to admin_departments_path
      end
    end   
  end
end
