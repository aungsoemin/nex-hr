ActiveAdmin.register OtherAllowance do

	menu parent: 'Payroll'

	permit_params :employee_id, :name, :amount, :allowance_month

	index do
	  selectable_column
	  id_column
	  column :employee
	  column :name
	  column :amount
	  column :allowance_month
	  actions
	end

	form do |f|
    f.inputs "New Other Allowance" do
		  f.input :employee
		  f.input :name
		  f.input :amount
		  f.input :allowance_month,:as => :datepicker
    end
    f.actions
	end


end