ActiveAdmin.register EmployeeHandbook do

  permit_params :name, :file, :is_published

  index do
    selectable_column
    id_column
    column :name
    column :file
    column :is_published
    actions
  end

  filter :name
  filter :is_published

  form do |f|
    f.inputs "New Department" do
      f.input :name
      f.input :file,  :as => :file
      f.input :is_published
    end
    f.actions
  end

  show do |a|
      attributes_table do
        row :id
        row :name
        row :file do
          if a.file.present?
            ("<a href ='#{a.file}' target='_blank'>#{a.file}</a>").html_safe
          end
        end
        row :created_at
        row :updated_at
      end
    end

  after_save do |employee_handbook|
    if employee_handbook.is_published
      @employee_handbooks = EmployeeHandbook.where.not(id: employee_handbook.id)
      @employee_handbooks.each do |hb|
        hb.update_column(:is_published, false)
      end
    end
  end

end
