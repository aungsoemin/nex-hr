ActiveAdmin.register AttendanceSetting do

menu parent: 'Attendance'

permit_params :start_time, :end_time, :week_days => []

  index do
    selectable_column
    id_column
    column :start_time
    column :end_time
    actions
  end

  show do |i|
    attributes_table do
      row :id
      row :start_time
      row :end_time
      row :week_days do
        AttendanceSetting::days.collect { |k,v| if (JSON.parse(i.week_days)).include?(v) then k.upcase end}.join(", ")
      end
    end
  end

  form do |f|

    f.inputs "New Attendance Setting" do
      
      # f.input :start_time, as: :time_picker
      # f.input :end_time, as: :time_picker
      # f.input :week_days, as: :check_boxes, collection: AttendanceSetting::days

      if f.object.new_record?
        f.input :start_time, as: :time_picker,:input_html => { :value => "10:00" }
        f.input :end_time, as: :time_picker,:input_html => { :value => "18:00" }
      else
        f.input :start_time, as: :time_picker
        f.input :end_time, as: :time_picker
      end
      f.input :week_days, as: :check_boxes, collection: AttendanceSetting::days

    end
    f.actions
  end

end