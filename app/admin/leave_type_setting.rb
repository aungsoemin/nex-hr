ActiveAdmin.register LeaveTypeSetting do
menu parent: 'Leave'
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :leave_type_id, :no_of_leave, :experience_year

index do
  selectable_column
  id_column
  column :name
  column :experience_year
  column :leave_type
  column :no_of_leave
  actions
end

show do |i|
  attributes_table do
    row :id
    row :name
    row :experience_year
    row :leave_type
    row :no_of_leave
  end
end

form do |f|
  f.inputs "New Leave Type Setting" do
  	f.input :name
    f.input :experience_year
    f.input :leave_type_id,:as => :select, :collection => LeaveType.all.map{|u| [ "#{u.name} - " + (u.is_paid == true ? "paid" : "unpaid"), u.id]}
    f.input :no_of_leave
  end
  f.actions
end

end
