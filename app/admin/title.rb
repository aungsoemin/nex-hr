ActiveAdmin.register Title do
config.batch_actions = false
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	menu parent: 'Employee'

	permit_params :name, :rank
	
	index do
		selectable_column
		id_column
		column :name
		column :rank
		actions
	  end

	  filter :name
	  filter :rank

	  form do |f|
		f.inputs "New Title" do
		  f.input :name
		  f.input :rank
		end
		f.actions
	  end

	  show do
	  attributes_table do
	  	row :id
	    row :name
	    row :rank
	    row :created_at
	    row :updated_at
	  end
	end

	controller do 
    def destroy
      employees = Employee.where("title_id = ? ","#{resource.id}").count
      puts employees
      if employees > 0
        flash[:error] = "You can't delete. #{employees} Employee(s) with the Title ID - #{resource.id} exist! "
        redirect_to :back
      else
      	Title.destroy_all(id:"#{resource.id}")
        flash[:notice] = "Delete Successful! "
        redirect_to admin_departments_path
      end
    end   
  end
end
