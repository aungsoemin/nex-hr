ActiveAdmin.register Leave do

menu parent: 'Leave'

permit_params :requested_date, :start_date, :end_date, :leave_type_id, :reason, :attachment, :employee_id, :first_approved_by_id, :second_approved_by_id, :leave_status_id, :is_half_day, :start_time, :end_time

  filter :employee
  filter :requested_date
  filter :start_date
  filter :end_date
  filter :leave_type
  filter :leave_status

	index do
    selectable_column
    id_column
    column :employee
    column :requested_date
    column :start_date
    column :end_date
    column :is_half_day
    column :leave_type
    column :leave_status
    actions
  end

  show do |i|
    attributes_table do
      row :id
      row :requested_date
      row :start_date
      row :end_date
      row :is_half_day
      row :start_time
      row :end_time
      row :leave_type
      row :reason
      row :attachment
      row :employee
      row :first_approved_by_id
      row :second_approved_by_id
      row :leave_status
    end
  end

  form do |f|
    f.inputs "New Leave" do
    	f.input :employee
      f.input :requested_date, as: :date_picker,:placeholder => "yyyy-mm-dd"
      f.input :start_date, as: :date_picker,:placeholder => "yyyy-mm-dd"
      f.input :end_date, as: :date_picker,:placeholder => "yyyy-mm-dd"
      f.input :is_half_day, :input_html => {
        :onchange => <<-JS
                     if(this.checked == true){
                       document.getElementById("leave_start_time").disabled = false;
                       document.getElementById("leave_end_time").disabled = false;
                     }
                     if(this.checked == false){
                       document.getElementById("leave_start_time").disabled = true;
                       document.getElementById("leave_end_time").disabled = true;
                     }
                     JS
      }
      if f.object.new_record?
        f.input :start_time, as: :time_picker,:input_html => { :disabled => true } 
        f.input :end_time, as: :time_picker,:input_html => { :disabled => true } 
      else
        if f.object.is_half_day == true
          f.input :start_time, as: :time_picker,:input_html => { :disabled => false } 
          f.input :end_time, as: :time_picker,:input_html => { :disabled => false } 
        else
          f.input :start_time, as: :time_picker,:input_html => { :disabled => true } 
          f.input :end_time, as: :time_picker,:input_html => { :disabled => true } 
        end
      end
      f.input :leave_type
      f.input :reason
      f.input :attachment
      f.input :first_approved_by
      f.input :second_approved_by
      f.input :leave_status
    end
    f.actions
  end

end
