ActiveAdmin.register AttendanceSheet do

menu parent: 'Attendance'
menu false
permit_params :employee_id, :start_time, :end_time, :date, :submit_by_id

  # CSV import
  active_admin_importable do |model, hash|
    employee_code = '%04d' % hash[:employee_code].force_encoding('UTF-8') if hash[:employee_code].present?
    employee = Employee.where(:employee_code => employee_code).first
    hash[:employee_id] = employee.id
    hash[:start_time] = hash[:start_time].force_encoding('UTF-8') if hash[:start_time].present?
    hash[:end_time] = hash[:end_time].force_encoding('UTF-8') if hash[:end_time].present?
    hash[:date] = hash[:date].force_encoding('UTF-8') if hash[:date].present?
    submit_by_code= '%04d' % hash[:submit_by_code].force_encoding('UTF-8') if hash[:submit_by_code].present?
    submit_by = Employee.where(:employee_code => submit_by_code).first
    hash[:submit_by_id] = submit_by.id if submit_by.present?
    hash.delete(:employee)
    hash.delete(:employee_code)
    hash.delete(:submit_by)
    hash.delete(:submit_by_code)
    model.create!(hash)
  end

  # CSV export
  csv do
    column(:employee) {|obj| obj.employee.present? ? obj.employee.name : ''} 
    column(:employee_code) {|obj| obj.employee.present? ? obj.employee.employee_code.to_s : ''} 
    column :start_time 
    column :end_time
    column :date
    column(:submit_by) {|obj| obj.submit_by.present? ? obj.submit_by.name : ''}
    column(:submit_by_code) {|obj| obj.submit_by.present? ? obj.submit_by.employee_code.to_s : ''}
  end

	index do
    selectable_column
    id_column
    column :employee
    column :start_time
    column :end_time
    column :date
    column :submit_by
    actions
  end

  form do |f|
    f.inputs "New Attendance Sheet" do
		  f.input :employee
		  f.input :start_time, as: :time_picker
		  f.input :end_time, as: :time_picker
		  f.input :date
		  f.input :submit_by, :as => :select, collection: Employee.joins(:department).where("departments.code = 'mm'").map {|i| [i.name, i.id]}
    end
    f.actions
	end
end