//= require active_admin/base
//= require moment 
//= require fullcalendar

$(document).ready(function() {

    $('.calendar').fullCalendar({
     left: "prev,next today",
     center: "title"
    })

    $("#additional_detach_detach_month").datepicker( {
    	changeMonth: true,
	    changeYear: true,
	    viewMode: "years",
	    dateFormat: "yy-mm-01"
		})

		$("#other_allowance_allowance_month").datepicker( {
    	changeMonth: true,
	    changeYear: true,
	    viewMode: "years",
	    dateFormat: "yy-mm-01"
		})

		if ($("input[type='checkbox'][name='holiday[holiday_type]']:checked").val() == true){
			$('#holiday_fix_holiday_input').show();
		 	$('#holiday_date_input').hide();
		 	$('#holiday_name_input').hide();
		}else{
			$('#holiday_fix_holiday_input').hide();
		 	$('#holiday_date_input').show();
		 	$('#holiday_name_input').show();
		}
		
});